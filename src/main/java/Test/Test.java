package Test;

public class Test {

    int count = 0;

    Test(int count){
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static void main(String[] args) {
        int count = 2;
        Test test = new Test(count);
        System.out.println(test.getCount());
        count++;
        test.setCount(count);
        System.out.println(test.getCount());
    }
}
