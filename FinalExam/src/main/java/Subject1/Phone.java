package Subject1;

public class Phone implements User {

    GPSModule[] GPSModule = new GPSModule[1];
    Button[] Button = new Button[10];

    Phone() {
        Battery Battery = new Battery("Li-ion");
        Display Display = new Display();
        GPSModule[0] = new GPSModule();
        Button[0] = new Button(1);
        Button[1] = new Button(2);
        Button[2] = new Button(3);
        Button[3] = new Button(4);
        Button[4] = new Button(5);
        Button[5] = new Button(6);
        Button[6] = new Button(7);
        Button[7] = new Button(8);
        Button[8] = new Button(9);
        Button[9] = new Button(0);
    }

    @Override
    public void call() {
        System.out.println("Ring Ring!");
    }
}

class GPSModule {
    String GPS;
}

class Button {
    int b;
    private Display display;

    Button(int b) {
        this.b = b;
    }
}

class Battery {
    String type;

    Battery(String type) {
        this.type = type;
    }
}

class Display {
    private String message;
}

interface User {
    void call();
}

