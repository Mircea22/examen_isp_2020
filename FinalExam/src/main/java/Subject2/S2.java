package Subject2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class S2 extends JFrame implements ActionListener {

    JTextField textField1;
    JTextField textField2;
    JButton b1;

    public S2(String s) throws IOException {

        super(s);

        textField1 = new JTextField("src/main/resources/File.txt");

        add(textField1);
        add(textField2);

        b1 = new JButton("Write");
        b1.addActionListener(this);
        add(b1);

        textField1.setBounds(30, 40, 450, 20);
        textField2.setBounds(30, 80, 450, 20);
        b1.setBounds(100, 240, 100, 30);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setLocation(250, 100);
        setVisible(true);
        setSize(600, 350);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        FileWriter myWriter = null;
        if (e.getSource() == b1) {
            String file = textField1.getText();
            try {
                myWriter = new FileWriter(file);
                BufferedWriter result = new BufferedWriter(myWriter);
                result.write(textField2.getText());
                result.newLine();
                result.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        }

    }

    public static void main(String s[]) throws IOException {
        new S2("S2");
    }
}
